from subprocess import Popen, call, PIPE, DEVNULL
from distutils.spawn import find_executable

class Wifi:
	def __init__(self, args):
		self.internet_interface = args.internet_interface
		self.wifi_interface = args.wifi_interface
		self.hotspot_ap_name = args.hotspot_ap_name
		self.hotspot_password = args.hotspot_password

	def start(self):
		required_programs = ["create_ap", "dnsmasq", "iptables", "haveged", "hostapd"]
		for program in required_programs:
			assert find_executable(program), "Please install " + program + " script before running this."
		self.stop()

		args = [self.wifi_interface, self.internet_interface, self.hotspot_ap_name]
		if self.hotspot_password != None:
			args.extend([self.hotspot_password])
		Popen(["sudo", "create_ap"] + args)
		print("If the configuration is correct, the hotspot AP should be up.")

	def stop(self):
		if self.isRunning():
			call(['sudo', 'killall', 'create_ap'])
		print("Wifi process has been stopped.")

	def isRunning(self):
		ps = Popen(['ps', '-cax'], stdout=PIPE)
		grep = Popen(['grep', 'create_ap'], stdin=ps.stdout, stdout=DEVNULL)
		grep.wait()

		return grep.returncode == 0

	def isInternetInterface(self):
		ifconfig = Popen(['ifconfig'], stdout=PIPE)
		grep = Popen(['grep', self.internet_interface], stdin=ifconfig.stdout, stdout=DEVNULL)
		grep.wait()

		return grep.returncode == 0
