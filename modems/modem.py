from subprocess import Popen, call, PIPE, DEVNULL
from tempfile import mkstemp
from distutils.spawn import find_executable
from os import remove
from time import sleep

class Modem:
	def __init__(self, args):
		self.init_count = 0
		self.apn = args.apn
		self.baud_rate = 9600
		self.phone_number = args.phone_number
		self.username = args.username
		self.password = args.password
		self.modem_type = args.modem_type
		self.modem_model = args.modem_model
		self.pin = args.pin
		self.dev = args.dev

	def createConfigFile(self):
		(fd, path) = mkstemp()
		file = open(path, "w")

		string = "[Dialer Defaults]\n"
		if self.pin != None:
			string += self.initLine("AT+CPIN=\"" + self.pin + "\"")
		string += self.initLine("AT+CGDCONT=1,\"IP\",\"" + self.apn + "\"")
		string += "Baud = " + str(self.baud_rate) + "\n"
		string += "Phone = " + self.phone_number + "\n"
		string += "Username = \"" + self.username + "\"\n"
		string += "Password = \"" + self.password + "\"\n"
		string += "Modem = " + self.dev + "\n"

		file.write(string)
		print("Config commands were written to ", path)
		file.close()

		return path

	def start(self):
		# wvdial is the program that does the actual 3G connection
		assert find_executable("wvdial"), "Please install wvdial packet before running this."
		self.stop()
		path = self.createConfigFile()
		Popen(["sudo", "wvdial", "--config=" + path])
		print("If the configuration is correct, modem should be up in a few seconds.")

	def stop(self):
		if self.isRunning():
			call(['sudo', 'killall', 'wvdial'])
		print("Modem process has been stopped.")

	def isRunning(self):
		ps = Popen(['ps', '-cax'], stdout=PIPE)
		grep = Popen(['grep', 'wvdial'], stdin=ps.stdout, stdout=DEVNULL)
		grep.wait()

		return grep.returncode == 0

	def persistent_switch(self, persistent_action=None):
		fileName = "70-" + self.modem_type + "_" + self.modem_model + "_switch.rules"
		filePath = "/etc/udev/rules.d/" + fileName

		if persistent_action == "delete":
			remove(filePath)
			print("Persistent switch configs were deleted from: " + filePath)
		else:
			file = open(filePath, "w+")
			line = "ACTION==\"add\", SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"" + self.vendor_id + "\", " + \
				"ATTRS{idProduct}==\""+ self.storage_product_id +"\", RUN+=\"/bin/bash /bin/switch.sh" + "\""	
			file.write(line + "\n")
			file.close()
			print("Persistent switch configs were written to: " + filePath)
		Popen(["sudo", "udevadm", "control", "--reload-rules"])
		Popen(["sudo", "udevadm", "trigger"])

	def persistent_connect(self, persistent_action=None):
		fileName = "70-" + self.modem_type + "_" + self.modem_model + "_connect.rules"
		filePath = "/etc/udev/rules.d/" + fileName

		if persistent_action == "delete":
			remove(filePath)
			print("Persistent connect configs were deleted from: " + filePath)
		else:
			file = open(filePath, "w+")
			line = "ACTION==\"add\", SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"" + self.vendor_id + "\", " + \
				"ATTRS{idProduct}==\""+ self.modem_product_id +"\", RUN+=\"/bin/bash /bin/connect.sh" + "\""
			file.write(line + "\n")
			file.close()
			print("Persistent connect configs were written to: " + filePath)
		Popen(["sudo", "udevadm", "control", "--reload-rules"])
		Popen(["sudo", "udevadm", "trigger"])

	def persistent_wifi(self, persistent_action=None):
		fileName = "70-" + self.modem_type + "_" + self.modem_model + "_wifi.rules"
		filePath = "/etc/udev/rules.d/" + fileName

		if persistent_action == "delete":
			remove(filePath)
			print("Persistent wifi configs were deleted from: " + filePath)	
		else:
			file = open(filePath, "w+")
			line = "ACTION==\"add\", SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"" + self.vendor_id + "\", " + \
				"ATTRS{idProduct}==\""+ self.modem_product_id +"\", RUN+=\"/bin/bash /bin/wifi.sh" + "\""
			file.write(line + "\n")
			file.close()
			print("Persistent wifi configs were written to: " + filePath)
		Popen(["sudo", "udevadm", "control", "--reload-rules"])
		Popen(["sudo", "udevadm", "trigger"])

	def switch(self):
		Popen(["sudo", "usb_modeswitch", "-v", self.vendor_id, "-p", self.storage_product_id, "-M", self.switch_message])
		file = open("/sys/bus/usb-serial/drivers/option1/new_id")
		Popen(["sudo", "echo", self.vendor_id, self.modem_product_id], stdout=file)
		while True:
			print("Waiting for switching to be done")
			ps = Popen(["lsusb"], stdout=PIPE)
			grep = Popen(['grep', self.modem_product_id], stdin=ps.stdout, stdout=DEVNULL)
			grep.wait()
			if grep.returncode == 0:
				break
			sleep(1)

	def initLine(self, string):
		init_string = "Init" + str(self.init_count) + " = " + string + "\n"
		self.init_count += 1

		return init_string
