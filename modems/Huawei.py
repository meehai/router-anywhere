from modems.modem import Modem

# class for Huawei E3131 modem
class E3131(Modem):
	def __init__(self, args):
		super().__init__(args)
		self.vendor_id = "12d1"
		self.storage_product_id = "14fe"
		self.modem_product_id = "1506"
		self.switch_message = "55534243123456780000000000000011062000000100000000000000000000"